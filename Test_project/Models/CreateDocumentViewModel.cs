﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Test_project.Models
{
    public class CreateDocumentViewModel
    {
        
        public string Title { get; set; }
        public string Description { get; set; }
        public string PathFile { get; set; }

        public List<string> Users { get; set; }



    }
}