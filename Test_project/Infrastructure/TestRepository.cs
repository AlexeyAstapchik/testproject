﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test_project.Models;
using System.Web.Mvc;

namespace Test_project.Infrastructure
{
    public class TestRepository : IAppRepository
    {

        ApplicationDbContext _db = new ApplicationDbContext();
        public void AddAssignment(IEnumerable<ApplicationUser> user, ApplicationUser author, Document doc)
        {
            Assignment assig = new Assignment();
            assig.AppUser = author;
            assig.Doc = doc;
            assig.DocType = DocumentType.Authored;
            author.Assignments.Add(assig);
            foreach (var temp in user)
            {
                Assignment ag = new Assignment();
                ag.DocType = DocumentType.Unread;
                ag.Doc = doc;
                ag.AppUser = author;
                temp.Assignments.Add(ag);
            }
            _db.SaveChanges();
        }


        

        public void CreateAssignment(IEnumerable<ApplicationUser> user, string Title, string Description, string mpth, HttpPostedFileBase file, ApplicationUser author)
        {
            var doc = new Document()
            {
                Title = Title,
                Description = Description,
                PathFile = file == null ? null : Utils.SaveFileToDisk(file)

            };
            _db.Documents.Add(doc);
             _db.SaveChanges();
            // doc = _db.Documents.Single(d => d.DocumentId == doc.DocumentId);
            Assignment assig = new Assignment();
            assig.AppUser = author;
            //assig.Doc = doc;
            assig.DocType = DocumentType.Authored;
            //author.Assignments.Add(assig);
            doc.Assignments.Add(assig);
            foreach (var temp in user)
            {
                Assignment ag = new Assignment();
                ag.DocType = DocumentType.Unread;
                //ag.Doc = doc;
                ag.AppUser = temp;
                //temp.Assignments.Add(ag);
                doc.Assignments.Add(ag);
            }
            
            _db.SaveChanges();

        }

        public void AddDocument(Document doc, string author)
        {
            var user = GetUserByName(author);
            Assignment asg = new Assignment();
            asg.Doc = doc;
            asg.AppUser = user;
            asg.DocType = DocumentType.Authored;
            user.Assignments.Add(asg);
            //user.Assignments.Add();
            _db.SaveChanges();

        }

        //public ICollection<ApplicationUser> UserUnreadDocument(string name_doc)
        //{
        //    var doc = GetDocByName(string name_doc);

        //}

        public void ChangeAssignmentState(ApplicationUser user, string assignmentId)
        {
            int id;
            if (!int.TryParse(assignmentId, out id))
                return;
            var assignment = user.Assignments.SingleOrDefault(a => a.Id == id);
            if (assignment == null)
                return;
            if (assignment.DocType == DocumentType.Read)
            {
                assignment.DocType = DocumentType.Unread;
            }

            else
            {
                assignment.DocType = DocumentType.Read;
            }

            _db.SaveChanges();


        }

        public ICollection<Assignment> readDocument(string name)
        {
            var us = _db.Users.SingleOrDefault(a => a.UserName == name);
            var read = us.Assignments.Where(c => c.DocType == DocumentType.Read);
            return read.ToList();

        }

        public ICollection<Assignment> UnreadDocument(string name)
        {
            var us = _db.Users.SingleOrDefault(a => a.UserName == name);
            var read = us.Assignments.Where(c => c.DocType == DocumentType.Unread);
            return read.ToList();

        }

        

        public ApplicationUser GetUserByAsg(Assignment asg)
        {
            return _db.Users.FirstOrDefault(b => b.Assignments == asg);
        }
        public Assignment GetAssigmentByDoc(Document doc, DocumentType type)
        {
            return _db.Assignments.FirstOrDefault(a => a.Doc == doc && a.DocType == type);
        }
        public ICollection<ApplicationUser> GetAllUsers()
        {
            return _db.Users.ToList();
        }

        public ApplicationUser GetUserByName(string name)
        {
            return _db.Users.FirstOrDefault(u => u.UserName == name);
        }


        public ICollection<Assignment> GetAssignmentsByPath(string path, string name)
        {
            return _db.Assignments.Where(a => a.Doc.PathFile == path && a.DocType !=0 && a.Doc.Title == name).ToList();

        }

       




        public ICollection<ApplicationUser> GetAllUsersQ(string name)
        {
            return _db.Users.Where(t => t.UserName != name).ToList();
        }


    }
}