﻿$(document).ready(function () {
    $("#submit_id").click(function () {
        var selected = [];
        $('#body_checks input:checked').each(function (index, value) {
            selected.push($(this).parent().children("div").text());
        });
    });
    $('#select_all').change(function () {
        var checkboxes = $(this).closest('form').find(':checkbox');
        if ($(this).is(':checked')) {
            checkboxes.prop('checked', true);
        } else {
            checkboxes.prop('checked', false);
        }
    });
});