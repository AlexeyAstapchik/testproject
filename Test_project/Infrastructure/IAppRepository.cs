﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Test_project.Models;

namespace Test_project.Infrastructure
{
    public interface IAppRepository
    {
        void AddDocument(Document doc, string author);
        void ChangeAssignmentState(ApplicationUser user, string assignmentId);
        void AddAssignment(IEnumerable<ApplicationUser> user, ApplicationUser author, Document doc);
        void CreateAssignment(IEnumerable<ApplicationUser> user, string Title, string mpth, string Description, HttpPostedFileBase file, ApplicationUser author);

        ICollection<ApplicationUser> GetAllUsers();
        ApplicationUser GetUserByName(string name);
        ICollection<Assignment> readDocument(string name);
        ICollection<Assignment> UnreadDocument(string name);
        ICollection<ApplicationUser> GetAllUsersQ(string name);
        ICollection<Assignment> GetAssignmentsByPath(string path, string name);


        ApplicationUser GetUserByAsg(Assignment asg);


    }
}
