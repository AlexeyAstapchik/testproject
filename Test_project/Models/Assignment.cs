﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test_project.Models
{
    public class Assignment
    {
        public int Id { get; set; }
        public virtual Document Doc { get; set; }
        public DocumentType DocType { get; set; }
        
        public virtual ApplicationUser AppUser { get; set; }

    }
}