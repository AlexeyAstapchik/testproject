﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test_project.Infrastructure;

namespace Test_project.Controllers
{
    public class BaseController: Controller
    {
        [Inject]
       protected IAppRepository Repository { get; set; }
        public BaseController()
        {
            Repository = DependencyResolver.Current.GetService<IAppRepository>();
        }
        
    }
}