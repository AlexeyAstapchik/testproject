﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Test_project.Startup))]
namespace Test_project
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
