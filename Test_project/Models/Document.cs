﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test_project.Models
{
    public class Document
    {
        public Document()
        {
            Assignments = new List<Assignment>();
        }
        public int DocumentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string PathFile { get; set; }
        public virtual ICollection<Assignment> Assignments { get; set; }

    }

    public enum DocumentType
    {
        Authored,
        Read,
        Unread
    }
}