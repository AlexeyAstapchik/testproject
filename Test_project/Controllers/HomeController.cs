﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test_project.Infrastructure;
using Test_project.Models;

namespace Test_project.Controllers
{
    
    public class HomeController : BaseController
    {

        


        public ActionResult Index()
        {
            if (!Request.IsAuthenticated)
                return View("About");
           
            var assigments = Repository.GetUserByName(User.Identity.Name).Assignments.Where(a => a.DocType == DocumentType.Unread || a.DocType == DocumentType.Read);
            return View(assigments);
            //return View(doc);
            
        }

        public ActionResult DownloadFile(string name, string path)
        {
            string filename = Server.MapPath("/UploadedFiles/" + path);
            var abc = System.IO.Path.GetExtension(filename);
            string contentType = "application/pdf"; 
            string downloadName = name + abc;
            return File(filename, contentType, downloadName);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            if (!Request.IsAuthenticated)
                return View("About");
            var assigments = Repository.GetUserByName(User.Identity.Name).Assignments.Where(a => a.DocType == DocumentType.Authored);
        
            return View(assigments);
        }
    }
}