﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Test_project.Infrastructure
{
    public class Utils
    {
        public static string SaveFileToDisk(HttpPostedFileBase img, string mapPath)
        {
            var r = new Random();
            Directory.CreateDirectory(mapPath + "/UserContent/");
            var ext = "." + img.FileName.Split('.').Last();
            var name = r.Next(1000000, Int32.MaxValue).ToString() +
                        r.Next(1000000, Int32.MaxValue).ToString() + ext;
            var fName = mapPath + "UserContent\\" + name;
            img.SaveAs(fName);
            return name;
        }
        public static string SaveFileToDisk(HttpPostedFileBase file)
        {
            
                string path = AppDomain.CurrentDomain.BaseDirectory + "UploadedFiles/";
                Directory.CreateDirectory(path);
                string filename = Path.GetFileName(file.FileName);
                if (filename != null) file.SaveAs(Path.Combine(path, filename));
            
            return filename;
        }
    }
}