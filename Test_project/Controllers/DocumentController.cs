﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test_project.Infrastructure;
using Test_project.Models;

namespace Test_project.Controllers
{
    
    public class DocumentController : BaseController
    {
        
        // GET: Document
        public ActionResult Index()
        {
            return View();
        }

        // GET: Document/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Document/Create
        public ActionResult Create()
        {
            if (!Request.IsAuthenticated)
                return RedirectToAction("About","Home");
            var  name = User.Identity.Name;
            var qwe = new CreateDocumentViewModel
            {
                Users = Repository.GetAllUsersQ(name).Select(t => t.UserName).ToList()
                
            };
            return View(qwe);
        }

        // POST: Document/Create
        [HttpPost]
        public ActionResult Create(CreateDocumentViewModel viewModel, string[] Checkboxes, HttpPostedFileBase file)
        {
            
            var appUser = new List<ApplicationUser>();
            foreach(var user in Checkboxes)
            {
                appUser.Add(Repository.GetUserByName(user));
            }
            var author = Repository.GetUserByName(User.Identity.Name);
            Repository.CreateAssignment(appUser, viewModel.Title, viewModel.Description, viewModel.PathFile, file, author);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult ChangeStatus(string assignmentId)
        {
            Repository.ChangeAssignmentState(Repository.GetUserByName(User.Identity.Name), assignmentId);
            return RedirectToAction("Index","Home");
        }

        public ActionResult ShowDocumentStatus(string path, string name)
        {
            if (!Request.IsAuthenticated)
                return RedirectToAction("About", "Home");
            var ass = Repository.GetAssignmentsByPath(path, name);
            
            
            return View("ShowDocumentStatus",ass);
        }

        public ActionResult ViewDocument()
        {
            return View();
        }

        // GET: Document/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Document/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Document/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Document/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
